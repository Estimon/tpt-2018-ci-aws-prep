# create vm in the cloud

- create aws ec2 instance
   - use latest ubuntu
   - micro instance
   - add public ip
- one done try to connect to machine using ssh

# add gitlab runner

 - install gitlab runner
   - https://docs.gitlab.com/runner/install/
      - https://docs.gitlab.com/runner/install/linux-repository.html 

 - register runner for the project
   - https://docs.gitlab.com/runner/register/index.html
   - under your project `Settings` > `CI / CD` > `Runners` (Set up a specific Runner manually)
     - for tags use `tpt-aws-shell`
     - and type=`shell`

 - add `.gitlab-ci.yml` using runner in aws, d59b3606fc2423e54d67b57c9d5303d530eef890
   - https://docs.gitlab.com/ce/ci/yaml/#tags


# install docker-ce in ec2

install docker-ce into your ec2 machine as instructed in docker readme
 > https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce

 - once done run in ec2 for testing: `sudo docker run --rm hello-world`

 - add same command also into ci to test in docker this way, c4ef0a062eff170ecfeddb07118ee4ec13efb678
   -> as you can see you ended up with permission error, https://gitlab.com/eritikass/tpt-2018-ci-aws-prep/-/jobs/151810702
   
add both your default user in ec2 (ubuntu) and gitlab-runner into docker group, so they would have access to run docker.
 > https://docs.docker.com/install/linux/linux-postinstall/
 
```bash
# add default user in machine
sudo usermod -aG docker ubuntu
# you may need to log out/in for this to have effect

# add gitlab-runner user
sudo usermod -aG docker gitlab-runner
```

# deploy code with docker using ci

> **NB**: make sure gitlab project is public for this stage to success!
>   - `Settings` -> `General` -> `Permissions` -> `Project visibility`=`Public`

> **NB**: Open  port 80 for your aws ec2 machine

 - add simple website and docker file for it, cb1b3376f730f42da2132a18051ba8d4eb916ef1
 - build docker image in ci, bbc27d0c2b8c25e175df1990db314a7436ef5818
   - https://docs.gitlab.com/ce/ci/docker/using_docker_build.html
      - https://docs.docker.com/engine/reference/commandline/build/
 - deploy/run docker image with gitlab ci, d1b6d63ab4a38ec711d87dc6d0c14c5fdf3d04d6
